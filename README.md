## What is this repo for?
This repo aims to help you understand how to automate tests for Mobile apps using Appium as the automation driver. 
It's a mix of guidelines, code snippets and challenging exercises.  

There are three (3) main packages you should look at:

* `test/java/examples`: You'll find code snippets for most of the concepts we'll cover during the training.
* `test/java/exercises`: This is where weekly exercises will be posted. Solutions will be here as well. 
* `test/java/workshop[n]`: Remember what we did last workshop? You'll find it here.

### Clone this repo
Make sure you have Appium requirements installed in your local dev environment. Also make sure you have the follloiwing AVDs installed:

| AVD Name | Android OS version | 
| --------|---------|
| Pixel_3_XL_API_29  | 10.0   |
| Pixel_3a_XL_API_28 | 9.0 |

Clone this repo by running the following command:
```
git clone https://pgsapo@bitbucket.org/pgsapo/mobile-test-automation-workshops.git
```
To install the project's dependencies just run 
```
gradle
``` 
from your command line. If you are using Intellij the just use the "Gradle" plugin 
to build the project or if you are using Eclipse then use the Gradle plugin for Eclipse called [Buildship](https://marketplace.eclipse.org/content/buildship-gradle-integration#group-details)

### Documentation
Every example, exercise and workshop session has its own README file where you'll find insigths so we encourage you to read them first.
Please let us know if you think the documentation lacks of concrete information.
