## Exercise
-Week of 03.02 to 07.02-
### Clone the repo
1. Clone this repository if you haven't done it yet. Just open your terminal and execute:
```
git clone https://pgsapo@bitbucket.org/pgsapo/mobile-test-automation-workshops.git
```
2. Once the repo has been cloned into your local dev environment, open the project with the IDE of your preference.
3. Work on this file: `src/test/java/exercises/_1/NavigationTest.java`
4. Commit your changes as you go.

### Instructions
Use the class "NavigationTest" as a template and write a script to automate the following user journey for the app 
"VodQA-Android". The ".apk" file can found be here: "/src/test/resources"

1. Tap/click on "Views"
2. Tap/click on "Controls"
3. Tap/click on "Dark Theme"
4. Tap/click on "2. Dark Theme"
5. Tap/click on "Checkbox 1"
6. Tap/click on "Checkbox 2"
7. Tap/click on "RadioButton 2"
8. Pick the planet "Mars"


![Views](images/1.png)
![Controls](images/2.png)
![Dark Theme](images/3.png)
![Mars](images/4.png)
