## Test setup/pre-conditions

### Context
Test runners like JUnit or TestNG define ways to anotate methods that will perform setup actions for your tests.
Usually annotations are used to "mark" these methods and because these methods will get executed before the methods 
representing your tests, the annotation starts with the keyword "Before".
e.g. Junit uses `@Before` and `@BeforeAll`
e.g. TestNG uses `@BeforeMethod`, `@BeforeTest`, `@BeforeClass`, etc.

When using automation tools like Appium, it is mandatory to explicity set the mobile automation capabilities to be used, 
so the "Before" methods are a good place to define these capabilities.  

### Capabilities
Capabilities are just a set of properties that Appium will take to instantiate an automation driver. The following are the most basic capabilities we'll use:

```json
{
  "automationName": "uiautomator2",
  "platformName": "Android",
  "platformVersion": "10",
  "app": "$HOME/projects/mobile-test-automation-workshops/src/test/resources/ApiDemos-debug.apk",
  "deviceName": "Pixel_3_XL_API_29",
  "avd": "Pixel_3_XL_API_29",
  "autoLaunch": true
}
```

* The "avd" capability is used so Appium can spin up the AVD (Android Virtual Device) you want use automatically. If this capability is not specified you'll need to spin up the emulator manually.
* The "autoLaunch" capability is used to install the App under test automatically. Without it, you'll have to install the app manually.    

### The setup method
You'll see that the test in this package uses the annotation `@Before` to tell Appium what are the desired capabilities we want to use.
```java
@Before
public void setUp() throws MalformedURLException {
    DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
    desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AUTOMATION_NAME);
    desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PLATFORM_NAME);
    desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, PLATFORM_VERSION);
    desiredCapabilities.setCapability(MobileCapabilityType.APP, APP);
    desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, DEVICE_NAME);
    desiredCapabilities.setCapability("avd", AVD);
    desiredCapabilities.setCapability("autoLaunch", AUTO_LAUNCH);
    desiredCapabilities.setCapability("appWaitDuration", APP_WAIT_DURATION);
    String url = "http://0.0.0.0:4723/wd/hub";
    driver = new AndroidDriver<>(new URL(url), desiredCapabilities);
}
``` 
Basically, the class "DesiredCapabilities" holds a hash map where the keys are capability's key word and the values are the actual capability's value we want to use. 
Once we have define the capabilities we need to instantiate an Android driver by passing these capabilities. The "url" variable points to our aleady started Appium Server.