package examples.testSetUp;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class DesiredCapabilitiesSetUp {

    AndroidDriver<MobileElement> driver;
    static final String AUTOMATION_NAME = "uiautomator2";
    static final String PLATFORM_NAME = "Android";
    static final String PLATFORM_VERSION = "10";
    static final String APP = new File("src/test/resources/ApiDemos-debug.apk").getAbsolutePath();
    static final String DEVICE_NAME = "Pixel_3_XL_API_29";
    static final String AVD = "Pixel_3_XL_API_29";
    static final String AUTO_LAUNCH = "True";
    static final String APP_WAIT_DURATION = "3000";


    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AUTOMATION_NAME);
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PLATFORM_NAME);
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, PLATFORM_VERSION);
        desiredCapabilities.setCapability(MobileCapabilityType.APP, APP);
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, DEVICE_NAME);
        desiredCapabilities.setCapability("avd", AVD);
        desiredCapabilities.setCapability("autoLaunch", AUTO_LAUNCH);
        desiredCapabilities.setCapability("appWaitDuration", APP_WAIT_DURATION);
        String url = "http://0.0.0.0:4723/wd/hub";
        driver = new AndroidDriver<>(new URL(url), desiredCapabilities);
    }

    @Test
    public void testSetup() {
        System.out.println("This is where testing happens");
    }

    @After
    public void tearDown(){
        driver.quit();
    }


}
