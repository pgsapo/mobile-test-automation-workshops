## Locating Elements

### Context
In order to take control over a mobile element, Appium needs to find it first. The structure of elements is hierarchical, like an N-ary tree or like the DOM interface for Web platforms, 
and there are several ways to locate elements. We encourage you to use and promote accessibility ids over any other strategy.

Please take a look at the following posts where you'll find better explanation about locator strategies:

* http://appium.io/docs/en/writing-running-appium/finding-elements/
* https://appiumpro.com/editions/60
* https://saucelabs.com/blog/advanced-locator-strategies

## Actions

### Click
Since we already know how to locate an element, we can now take control over it. You could click/tap all the elemets once successfully located.
The method name to use is `click();` 
Please find more information abut it here: http://appium.io/docs/en/commands/element/actions/click/

### Send keys
//ToDo

### Clear
//ToDo