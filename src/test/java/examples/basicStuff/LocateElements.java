package examples.basicStuff;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class LocateElements {

    AndroidDriver<MobileElement> driver;
    static final String AUTOMATION_NAME = "uiautomator2";
    static final String PLATFORM_NAME = "Android";
    static final String PLATFORM_VERSION = "10";
    static final String APP = new File("src/test/resources/ApiDemos-debug.apk").getAbsolutePath();
    static final String DEVICE_NAME = "Pixel_3_XL_API_29";
    static final String AVD = "Pixel_3_XL_API_29";
    static final String AUTO_LAUNCH = "True";
    static final String APP_WAIT_DURATION = "3000";
    static final String APP_ACTIVITY = ".ApiDemos";
    static final String PACKAGE = "io.appium.android.apis";


    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AUTOMATION_NAME);
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PLATFORM_NAME);
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, PLATFORM_VERSION);
        desiredCapabilities.setCapability(MobileCapabilityType.APP, APP);
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, DEVICE_NAME);
        desiredCapabilities.setCapability("avd", AVD);
        desiredCapabilities.setCapability("autoLaunch", AUTO_LAUNCH);
        desiredCapabilities.setCapability("appWaitDuration", APP_WAIT_DURATION);
        desiredCapabilities.setCapability("appPackage", PACKAGE);
        desiredCapabilities.setCapability("appActivity", APP_ACTIVITY);
        String url = "http://0.0.0.0:4723/wd/hub";
        driver = new AndroidDriver<>(new URL(url), desiredCapabilities);
    }

    @Test
    public void locatingAnElement() {
        // This element is located by using their accessibility id and its XPath expression. Up to this point Appium will query the app
        // and if an element is not found it will be throw an exception.
        MobileElement viewElementbyId = this.driver.findElementByAccessibilityId("Views");
        System.out.println(viewElementbyId.getText());

        MobileElement viewElementbyXpath = this.driver.findElementByXPath("//android.widget.TextView[@content-desc=\"Views\"]");
        System.out.println(viewElementbyXpath.getText());
    }

    @Test
    public void locatingABunchOfElements() {
        // You can query all the elements that share a particular property. In this case, lots of elements shares
        // the class name: "android.widget.TextView". The result ends up to be a list of elements or, in this case,
        // mobile elements.
        List<MobileElement> textViewElements = this.driver.findElementsByClassName("android.widget.TextView");
        // We could even iterate through the list of mobile elements and print its text and coordinates in the screen.
        textViewElements.forEach((textViewElement) -> System.out.println(String.format("%s | (%s,%s)",
                textViewElement.getText(),
                textViewElement.getLocation().getX(),
                textViewElement.getLocation().getY()
        )));
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}
