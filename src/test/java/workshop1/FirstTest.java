package workshop1;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class FirstTest {

    AndroidDriver<MobileElement> driver;
    static final String AUTOMATION_NAME = "uiautomator2";
    static final String PLATFORM_NAME = "Android";
    static final String PLATFORM_VERSION = "9";
    static final String APP = new File("src/test/resources/VodQA-Android.apk").getAbsolutePath();
    static final String DEVICE_NAME = "Pixel_3a_XL_API_28";
    static final String AVD = "Pixel_3a_XL_API_28";
    static final String AUTO_LAUNCH = "True";
    static final String APP_WAIT_DURATION = "3000";

    @Before
    public void setUp() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AUTOMATION_NAME);
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PLATFORM_NAME);
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, PLATFORM_VERSION);
        desiredCapabilities.setCapability(MobileCapabilityType.APP, APP);
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, DEVICE_NAME);
        desiredCapabilities.setCapability("avd", AVD);
        desiredCapabilities.setCapability("autoLaunch", AUTO_LAUNCH);
        desiredCapabilities.setCapability("appWaitDuration", APP_WAIT_DURATION);
        String url = "http://0.0.0.0:4723/wd/hub";
        driver = new AndroidDriver<>(new URL(url), desiredCapabilities);
    }

    @Test
    public void loginTest() throws InterruptedException {
        MobileElement txtUserName = driver.findElement(By.xpath("//android.widget.EditText[@content-desc='username']"));
        MobileElement txtPassword = driver.findElement(By.xpath("//android.widget.EditText[@content-desc='password']"));
        MobileElement btnLogin = driver.findElement(By.className("android.widget.Button"));
        txtUserName.sendKeys("admin");
        txtPassword.sendKeys("admin");
        btnLogin.click();
        new WebDriverWait(driver, 30).until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//android.widget.TextView[@text='Samples List']"))));
        Thread.sleep(5000);
    }

    @After
    public void tearDown() {
        driver.quit();
    }

}
